import React, {useState,useEffect} from 'react';
import { View,Text,StyleSheet,FlatList,Image } from 'react-native';
import yelp from '../api/yelp';

const DetailScreen = ({navigation}) => {

    const [result,setResult] = useState(null);
    const id = navigation.getParam('id');

    const getresult = async (id) => {

        const reponse = await yelp.get(`/${id}`);

        setResult(reponse.data);
    };

    useEffect(()=>{

        getresult(id);
        
    },[]);

    if(!result){
        return null;
    }

    return (
        <View>
            <Text style={styles.name}>{result.name}</Text>
            <FlatList 
                data={result.photos}
                keyExtractor={(photo)=>photo}
                renderItem={({item})=>{
                    return (
                        <Image style={styles.image} source={{uri: item}} />
                    );
                }}
            />

        </View>
    );
};

const styles = StyleSheet.create({
    image: {
        width: 300,
        height: 300,
        borderRadius: 5,
        margin: 2
    },
    name: {
        fontSize: 18,
        fontWeight : 'bold',
        margin: 2
    }
});

export default DetailScreen;