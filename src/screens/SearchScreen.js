import React, {useState} from 'react';
import {View ,Text, StyleSheet,ScrollView} from 'react-native';
import SearchBar from '../components/SearchBar';
import useResults from '../hooks/useResults';
import ResultsLists from '../components/ResultsLists';

const SearchScreen = () =>{

    
    const [term,setTerm] = useState('');
    const [searchApi,error,results] = useResults();

    const filterResultsByPrice = (price) => {

        return results.filter(result => {

            return result.price === price;
        });
    }
    
    return (

        <>
            <SearchBar 

                term={term}
                onTermChange={setTerm}
                onTermeSubmited={()=>searchApi(term)}

                />
            {error ? <Text> { error }</Text> : null}
            <ScrollView>
                <ResultsLists  results={filterResultsByPrice('$')} title = "Cost Effective"/>
                <ResultsLists  results={filterResultsByPrice('$$')}  title= "Big Pricier" />
                <ResultsLists  results={filterResultsByPrice('$$$')} title= "Big Spender" />
            </ScrollView>
        </>
    );
}

const styles = StyleSheet.create({
    
});

export default SearchScreen;