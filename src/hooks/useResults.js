import {useState,useEffect} from 'react';
import yelp from '../api/yelp';

export default ()=> {

    const [results,setResults] = useState([]);
    const [error,setError] = useState('');

    const searchApi = async (searchTerm) => {
        try {
            const reponse = await yelp.get('/search',{

                params: {
                    term: searchTerm,
                    limit: 50,
                    location: 'san jose'
                }
            });
    
            setResults(reponse.data.businesses);
        } catch (error) {
            setError('Something went wrong');
        }

    };

    useEffect(()=>{searchApi('pasta')},[]);

    return [searchApi,error,results];
};