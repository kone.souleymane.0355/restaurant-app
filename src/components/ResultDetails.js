import React from 'react';
import {Text,View,StyleSheet,Image} from 'react-native';


const ResultDetails = ({result}) => {

    return(
        <View>
            <Image style={styles.image} source={{ uri: result.image_url }} />
            <Text style={styles.name}>{ result.name}</Text>
            <Text style={styles.rate}>{ result.rating} Starts, {result.review_count} Reviews</Text>
        </View>
    );
};

const styles = StyleSheet.create({

    image: {
        width: 250,
        borderRadius: 4,
        height: 120,
        marginLeft: 10
    },
    name: {
        fontSize: 15,
        fontWeight: 'bold',
        marginLeft: 10

    },
    rate: {
        color: 'gray',
        opacity: 0.7,
        marginLeft: 10

    }
});

export default ResultDetails;