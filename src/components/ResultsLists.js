import React from 'react';
import {Text,View,StyleSheet,FlatList,TouchableOpacity} from 'react-native';
import ResultDetails from './ResultDetails';
import { withNavigation } from 'react-navigation';

const ResultsLists = ({ title,results,navigation })=>{

    if(!results.length){
        return null;
    }

    return (
        <View>
            <Text style={styles.titleStyle}>{title}</Text>
            <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                keyExtractor={result=>result.id}
                data={results}
                renderItem={({item})=>{
                    return (
                        <View>
                            <TouchableOpacity onPress={()=>{navigation.navigate('Detail',{id: item.id })}}>
                                <ResultDetails result={item} />
                            </TouchableOpacity>
                        </View>
                    );
                }}
            />
        </View>
    );
};

const styles = StyleSheet.create({

    titleStyle: {
        fontSize: 18,
        marginVertical: 10,
        fontWeight: 'bold',
        marginLeft: 10

    }
});

export default withNavigation(ResultsLists);